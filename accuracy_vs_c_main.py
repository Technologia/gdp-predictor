from svr import *

#result_lst format: [<kernel>, <c>, <degree>, <indiv_country_acc>]
def get_average_accuracy(result_lst, ker, c):
  lst = [i[3] for i in result_lst if (i[0]==ker and i[1]==c)]
  avg = np.mean(lst)
  return avg

'''
output average accuracy for 30 countries with different kernels and c values
'''
if __name__ == "__main__":
  startTime = datetime.now()

  ker_lst = ['linear', 'poly']
  c_lst = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 50, 100]
  degree_lst = [2]
  
  final_results = []
  #run test on individual countries
  for x in range(0, len(test_file_list)):
    print "================================================================="
    print train_file_list[x] #print country name
    #populate train_set and test_set
    train_set = []
    test_set = []
    parse_text_file(train_file_list[x], test_file_list[x])
    #find the best hypothesis
    results = []
    for ker in ker_lst:
      for c in c_lst:
        for degree in degree_lst:
          print "kernel " + ker + " c " + str(c)
          best_h = get_best_h(ker, c, degree)
          accuracy = get_test_accuracy(best_h, ker, c, degree)
          results.append([ker, c, degree, accuracy])
    final_results.extend(results)
    #print results
    for result in results:
      print result

  print "********************************************************************"
  print "Final Results: "
  #print final results
  ultimate_results = []
  for ker in ker_lst:
    for c in c_lst:
      avg_acc = get_average_accuracy(final_results, ker, c)
      result = [ker, c, avg_acc]
      ultimate_results.append(result)
  for r in ultimate_results:
    print r

  #print out runtime
  print(datetime.now()-startTime)
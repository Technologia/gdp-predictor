#!/usr/bin/python
import numpy as np
import sys
#import matplotlib.pyplot as plt

cname = sys.argv[1]
handle1 = open(cname+'.txt')

with open(cname+"_knn.txt",'w') as f:
	for line in handle1:
		if 'Year' not in line:
			data = line.strip().split(';')
			for i in range(2,len(data)-1):
				f.write(data[i]+';')
			f.write('\n')

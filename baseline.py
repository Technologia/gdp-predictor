from svr import *

#convert a list of examples into a list of gdp values
def get_gdp_lst(data_set):
  return [i.y for i in data_set]

#outputs a tuple of (mean, standard dev)
def get_mean_std(gdp_lst):
  mean = np.mean(gdp_lst)
  std = np.std(gdp_lst)
  return (mean, std)

#output baseline accuracy
def get_baseline_accuracy(train_set, test_data):
  gdp_lst = get_gdp_lst(train_set)
  (mean, std) = get_mean_std(gdp_lst)

  test_Y = []
  pred_Y = []
  for e in test_data:
    test_Y.append(e.y)
    pred_y = np.random.normal(mean, std)
    pred_Y.append(pred_y)

  err_lst = []
  for j in range(len(pred_Y)):
    err_lst.append(calc_nonsqr_err(pred_Y[j], test_Y[j]))
  return np.average(err_lst)

if __name__ == "__main__":
  startTime = datetime.now()

  #populate train_set and test_set
  parse_text_file("data/30countries.train", "data/30countries.test")
  base_acc = get_baseline_accuracy(train_set, test_set)
  print "**************************************************"
  print "baseline accuracy: "
  print base_acc

  #print out runtime
  print(datetime.now()-startTime)
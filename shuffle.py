from random import shuffle

read_file_list = [
                 'project/aus.txt', 
                 'project/aut.txt',
                 'project/bel.txt',
                 'project/can.txt',
                 'project/che.txt',
                 'project/cyp.txt',
                 'project/cze.txt',
                 'project/deu.txt',
                 'project/dnk.txt',
                 'project/esp.txt',
                 'project/fin.txt',
                 'project/fra.txt',
                 'project/grc.txt',
                 'project/irl.txt',
                 'project/isl.txt',
                 'project/isr.txt',
                 'project/ita.txt',
                 'project/jpn.txt',
                 'project/kor.txt',
                 'project/lux.txt',
                 'project/mlt.txt',
                 'project/nld.txt',
                 'project/nor.txt',
                 'project/nzl.txt',
                 'project/prt.txt',
                 'project/svk.txt',
                 'project/svn.txt',
                 'project/swe.txt',
                 'project/usa.txt']

train_file_list = [
                   'data/aus.train', 
                   'data/aut.train',
                   'data/bel.train',
                   'data/can.train',
                   'data/che.train',
                   'data/cyp.train',
                   'data/cze.train',
                   'data/deu.train',
                   'data/dnk.train',
                   'data/esp.train',
                   'data/fin.train',
                   'data/fra.train',
                   'data/grc.train',
                   'data/irl.train',
                   'data/isl.train',
                   'data/isr.train',
                   'data/ita.train',
                   'data/jpn.train',
                   'data/kor.train',
                   'data/lux.train',
                   'data/mlt.train',
                   'data/nld.train',
                   'data/nor.train',
                   'data/nzl.train',
                   'data/prt.train',
                   'data/svk.train',
                   'data/svn.train',
                   'data/swe.train',
                   'data/usa.train']

test_file_list = [
                   'data/aus.test', 
                   'data/aut.test',
                   'data/bel.test',
                   'data/can.test',
                   'data/che.test',
                   'data/cyp.test',
                   'data/cze.test',
                   'data/deu.test',
                   'data/dnk.test',
                   'data/esp.test',
                   'data/fin.test',
                   'data/fra.test',
                   'data/grc.test',
                   'data/irl.test',
                   'data/isl.test',
                   'data/isr.test',
                   'data/ita.test',
                   'data/jpn.test',
                   'data/kor.test',
                   'data/lux.test',
                   'data/mlt.test',
                   'data/nld.test',
                   'data/nor.test',
                   'data/nzl.test',
                   'data/prt.test',
                   'data/svk.test',
                   'data/svn.test',
                   'data/swe.test',
                   'data/usa.test']

def combine_files(file_lst, write_file):
  lines = []
  for f in file_lst:
    lines.extend(parse_data(f))
  w_file = open(write_file, 'w')
  for l in lines:
    w_file.write(l+'\n')
  w_file.close()

#parse txt file into an array of lines
def parse_data(file_name):
  f = open(file_name, 'r')
  lines = []
  for line in f:
    lines.append(line.rstrip())
  lines = lines[1:]
  f.close()
  return lines

def create_shuffled_file(read_file_name, write_train_file, write_test_file):
  lines = parse_data(read_file_name)
  shuffle(lines)

  threshold = int(len(lines) * float(7) / 10)
  f = open(write_train_file, 'w')
  for x in range(0, threshold):
    f.write(lines[x]+"\n")
  f.close()

  f = open(write_test_file, 'w')
  for x in range(threshold, len(lines)):
    f.write(lines[x]+"\n")
  f.close()

if __name__ == "__main__":
  for x in range(0, len(read_file_list)):
    create_shuffled_file(read_file_list[x], train_file_list[x], test_file_list[x])
  
  # create_shuffled_file('data/30countries.txt', 'data/30countries.train', 'data/30countries.test')
  
from svr import *


'''
Parse the data file with all 30 countries combined, find the best
hypothesis with linear regression and c = 1, and output the accuracy
on test data set. '''

if __name__ == "__main__":
  startTime = datetime.now()

  best_c = 0.1
  best_ker = 'linear'
  best_deg = 1

  parse_text_file("data/30countries.train", "data/30countries.test")
  results = []
  best_h = get_best_h(best_ker, best_c, best_deg)
  accuracy = get_test_accuracy(best_h, best_ker, best_c, best_deg)
  results.append([best_ker, best_c, best_deg, accuracy])
  
  #print final results
  for result in results:
    print result

  #print out runtime
  print(datetime.now()-startTime)
#!/usr/bin/python
import numpy as np
import sys
#import matplotlib.pyplot as plt

cname = sys.argv[1]
handle1 = open(cname+'_Country_en_csv_v2.csv')
country = []
country.append(['indicator Name',"1961","1962","1963","1964","1965","1966","1967","1968","1969","1970","1971","1972","1973","1974","1975","1976","1977","1978","1979","1980","1981","1982","1983","1984","1985","1986","1987","1988","1989","1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014"])
row_num = [51, 89, 504, 692, 719, 723, 746, 753, 811, 1195, 1234, 1245]
gdp = []
i = 1
index = 1
for line in handle1:
	data = line.strip().split('",')
	del data[-1]
	for k in range(len(data)):
		data[k] = data[k].replace("\"","")
	if i in row_num:
		temp = []
		temp.append(data[2])
		for j in range(4,len(data)):
			if data[j] == "":
				temp.append(0.0)
			else:
				temp.append(float(data[j]))
		country.append(temp)
		index += 1
	if i == 824:
		gdp.append(data[3])
		for j in range(4,len(data)):
			if data[j] == "":
				gdp.append(0.0)
			else:
				gdp.append(float(data[j]))
	i += 1
gdp.append("")
 
for i in range(len(country)):
	start = 0
	end = 0
	for j in range(len(country[i])):
		if start == 0 and country[i][j] == 0:
			start = j
		elif end == 0 and country[i][j] != 0:
			end = j
		if end-start > 0 and start == 1:
			for k in range(start,end):
				country[i][k] = country[i][end]
			start = 0
			end = 0
		if end-start > 0 and start > 1:
			avg = (country[i][start-1]+country[i][end])/2.0
			for k in range(start,end):
				country[i][k] = avg
			start = 0
			end = 0
		if start > 1 and j == len(country[i])-1:
			for k in range(start, j+1):
				country[i][k] = country[i][start-1]

#print country

prev_gdp = gdp[1:]
prev_gdp.insert(0,"Previous Year's GDP(%)")
country.append(prev_gdp[:-1])


for i in range(1,len(country)):
	mean = np.average(country[i][1:])
	std = np.std(country[i][1:])
#	print mean
#	print std
	for j in range(1,len(country[i])):
#		print country[i][j]
		country[i][j] = (country[i][j]-mean)/std
#		print country[i][j]

#23 attri
year = 1
with open(cname+".txt",'w') as f:
	f.write("CountryCode; Year;GDP;")
	for i in range(1,len(country)):
		f.write("%s;" % country[i][0])
	for j in range(1,len(country[0])-1):
		yr = 1
		f.write("\n"+cname+";%s;%s;" % (country[0][j],gdp[j+1]))
		for k in range(1,len(country)):
			f.write("%s;" % country[k][j])

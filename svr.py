from sklearn.svm import SVR

import sys
from datetime import datetime

import itertools
import numpy as np
from math import *
from shuffle import *
from random import *

# a mapping from attribute index to attribute name
attr_list = ['Current account balance (% of GDP)', 
             'Stocks traded, total value (% of GDP)',
             'Research and development expenditure (% of GDP)', 
             'Household final consumption expenditure, etc. (annual % growth)',
             'Gross national expenditure (% of GDP)',
             'Exports of goods and services (annual % growth)',
             'Imports of goods and services (annual % growth)',
             'Trade (% of GDP)',
             'Inflation, GDP deflator (annual %)',
             'Unemployment, total (% of total labor force) (national estimate)',
             'Population growth (annual %)',
             'Urban population growth (annual %)',
             'Previous Year\'s GDP(%)']

train_set = [] # a list example objects
test_set = [] # a list example objects

class Example:
  # y is a float (GDP), 
  # xs is a list of attribute values
  def __init__(self, y, xs, year, country):
    self.y = float(y)
    self.xs = xs
    self.year = year
    self.country = country

'''parse and shuffle, populate train_set and test_set'''
def parse_shuffle_text_file(file_name):
  train_set = []
  test_set = []

  f = open(file_name, 'r')
  lines = [line.strip() for line in f]
  lines = lines[1:]
  f.close()
  shuffle(lines)
  
  threshold = len(lines) * 7 / 10

  lines_train = lines[0:threshold]
  lines_test = lines[threshold:len(lines)]

  # populate training set
  for line in lines_train:
    tokens = line.split(';')
    country = tokens[0]
    year = tokens[1]
    y = tokens[2]
    xs = []
    for i in range(3, len(tokens)-1):
      xs.append(float(tokens[i]))
    train_set.append(Example(y, xs, year, country))

  # populate test set
  for line in lines_test:
    tokens = line.split(';')
    country = tokens[0]
    year = tokens[1]
    y = tokens[2]
    xs = []
    for i in range(3, len(tokens)-1):
      xs.append(float(tokens[i]))
    test_set.append(Example(y, xs, year, country))


'''
populate train_set and test_set
'''
def parse_text_file(train_file, test_file):
  # read file into a list of lines
  f_train = open(train_file, 'r')
  f_test = open(test_file, 'r')

  lines_train = [line.strip() for line in f_train]
  lines_test = [line.strip() for line in f_test]

  # populate training set
  for line in lines_train:
    tokens = line.split(';')
    country = tokens[0]
    year = tokens[1]
    y = tokens[2]
    xs = []
    for i in range(3, len(tokens)-1):
      xs.append(float(tokens[i]))
    train_set.append(Example(y, xs, year, country))

  # populate test set
  for line in lines_test:
    tokens = line.split(';')
    country = tokens[0]
    year = tokens[1]
    y = tokens[2]
    xs = []
    for i in range(3, len(tokens)-1):
      xs.append(float(tokens[i]))
    test_set.append(Example(y, xs, year, country))

#helper for kFold
def make_new_data(h, example):
  new_example_x = []
  for i in range(len(h)):
    if h[i] == 1:
      new_example_x.append(example.xs[i])
  return (new_example_x, example.y)

#helper for kFold
#returns the error between the predicted value and the actual value
def calc_err(pred_val, actual_val):
  diff = pred_val - actual_val
  return diff * diff

def calc_nonsqr_err(pred_val, actual_val):
  diff = pred_val - actual_val
  return fabs(diff)

#train_set format is [Example1, Example2..];
#ker is the kernel to use
def kFold(h, k, ker, c, degree):
  subArraysX = [] #holds k subarrays
  subArraysY = []
  for i in range(k):
    subArraysX.append([])
    subArraysY.append([])
  for i in range(len(test_set)):
    (new_x, new_y) = make_new_data(h, train_set[i])
    subArraysX[i%k].append(new_x)
    subArraysY[i%k].append(new_y)

  fold_err_list = []
  for i in range(k):
    X_train = []
    Y_train = []
    X_validate = subArraysX[i]
    Y_validate = subArraysY[i]
    for j in range(k):
      if i != j:
        X_train.extend(subArraysX[j])
        Y_train.extend(subArraysY[j])
    svr = SVR(kernel = ker, C = c, degree = degree)
    svr.fit(X_train, Y_train)
    SVR_pred = svr.predict(X_validate)
    sub_err_list = []
    for j in range(len(SVR_pred)):
      sub_err_list.append(calc_err(SVR_pred[j], Y_validate[j]))
    fold_err_list.append(np.average(sub_err_list))
  return np.average(fold_err_list)

#helper for get_best_h
def string_to_list(i):
    return [int(x) for x in i]

#ker is the kernel
def get_best_h(ker, c, degree):
  cur_best_diff = sys.float_info.max
  cur_best_h = [0] * 13

  # generate all possible hypothesis
  for x in ("".join(seq) for seq in itertools.product("01", repeat=13)):
    if not (x=="0000000000000"):
      h = string_to_list(x)
      accuracy = kFold(h, 4, ker, c, degree)
      #print each hypothesis combination
      # print '['+ ', '.join(str(x) for x in h) +']' + str(accuracy)
      if accuracy < cur_best_diff:
        cur_best_diff = accuracy
        cur_best_h = h

  best_attr = []
  for i in range(len(cur_best_h)):
    if cur_best_h[i] == 1:
      best_attr.append(attr_list[i])
  print "------------------------------------------------------------"
  print "Best difference is " + str(cur_best_diff)
  print "Best hypothesis is " + ', '.join(str(x) for x in cur_best_h)
  print "Best attributes are " + ', '.join(str(x) for x in best_attr)
  return cur_best_h

#get test accuracy of one individual country with certain parameters
def get_test_accuracy(h, ker, c, degree):
  train_X = []
  train_Y = []
  test_X = []
  test_Y = []
  for example in train_set:
    (new_x, new_y) = make_new_data(h, example)
    train_X.append(new_x)
    train_Y.append(new_y)
  for example in test_set:
    (new_x, new_y) = make_new_data(h, example)
    test_X.append(new_x)
    test_Y.append(new_y)
  svr = SVR(kernel = ker, C = c, degree = degree)
  svr.fit(train_X, train_Y)
  SVR_pred = svr.predict(test_X)
  err_lst = []
  for j in range(len(SVR_pred)):
    err_lst.append(calc_nonsqr_err(SVR_pred[j], test_Y[j]))
  return np.average(err_lst)


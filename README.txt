Root directory main files:

svr.py -- implementation of our models and algorithms
baseline.py -- implementation of a baseline method (fast to run)
accuracy_vs_c_main.py -- outputs average test accuracy with respect to different C values (takes 6 hours to run)
30countries_main.py -- outputs the test accuracy using global datasets (defined in our report)
(slow to run)


====================================================================================================
data/ 

This folder contains all test and training data we used in our machine learning algorithms


====================================================================================================
project/

This folder contains all the raw data file before parsing and the python code for parsing the data.
Description of data files and python codes

Data Files

1. *_Country_en_csv_v2.csv
---> These are the data files that we obtained from the World Bank website.

2. *.txt (except svm.txt)
---> These text files are parsed from 1. These are parsed for SVM regression with 13 attributes. The format is CountryCode;Year;GDP;a1;a2;....;a13

3. *_svm.txt
---> These text files are parsed from 1. These are parsed for SVMRank with 13 attributes. The format is Weight qid:number 1:number 2:number ..... 13:number #Countycode, Year

4. *_gdp.txt
---> These text files contain gdp values of year from 1961 to 2013.

5. GDP_ordered.txt
---> This text file has ordered gdp values of the 29 countries.

6. *_knn.txt
---> data files for knn.

Python Files

1. data_parse.py
---> This python script parses one 1. data file, and generates corresponding 2. data file.

2. data_parse_svm.py
---> This python script parses one 1. data file, and generates corresponding 3. data file.

3. multiple_run.sh
--->This bash script run 1. and 2. python files for the 29 countries and generates 29 2. and 3. data files each.

4. gdp.py
---> This python script gets one 2. data file and generates corresponding 4. data file.

5. gdp.sh
--> this bash script runs 4. python script for all 29 countries.

6. knn.py
---> generates *_knn.txt files. 

7. knn.sh
---> run knn.py for all countries.
